require(naniar)

load(file = "data/opref_imp_I_phy_ACM.RData")

# 1. Stat desc
I_sante_phy <- c("I1_etat_sante", "I2_etat_sante_av_depart", "I3_prob_sante", "I4_limitation_prob_sante")
vis_miss(opref_df[, I_sante_phy])
vis_miss(opref.imp_df[, I_sante_phy])

# 2. Distributions marginales avant et après imputation
data.I1 <- opref_df %>% filter(!is.na(I1_etat_sante))
data.imp.I1 <- opref.imp_df

prop.table(table(data.I1$I1_etat_sante))
prop.table(table(data.imp.I1$I1_etat_sante))

data.I2 <- opref_df %>% filter(!is.na(I2_etat_sante_av_depart))
data.imp.I2 <- opref.imp_df

prop.table(table(data.I2$I2_etat_sante_av_depart))
prop.table(table(data.imp.I2$I2_etat_sante_av_depart))

data.I3 <- opref_df %>% filter(!is.na(I3_prob_sante))
data.imp.I3 <- opref.imp_df

prop.table(table(data.I3$I3_prob_sante))
prop.table(table(data.imp.I3$I3_prob_sante))

data.I4 <- opref_df %>% filter(!is.na(I4_limitation_prob_sante))
data.imp.I4 <- opref.imp_df

prop.table(table(data.I4$I4_limitation_prob_sante))
prop.table(table(data.imp.I4$I4_limitation_prob_sante))

# 3. Test du chi2 pour la distribution conjointe
# Tableau de contingence avant imputation
I_sante_phy <- c("I1_etat_sante", "I2_etat_sante_av_depart", "I3_prob_sante", "I4_limitation_prob_sante")
data.opref <- opref_df %>% filter(if_all(all_of(I_sante_phy), ~ !is.na(.)))
table_avant <- table(data.opref$I1_etat_sante, data.opref$I2_etat_sante_av_depart,
                     data.opref$I3_prob_sante, data.opref$I4_limitation_prob_sante)

# Tableau de contingence après imputation
table_apres <- table(opref.imp_df$I1_etat_sante, opref.imp_df$I2_etat_sante_av_depart,
                     opref.imp_df$I3_prob_sante, opref.imp_df$I4_limitation_prob_sante)

# Test du chi-carré
chisq.test(table_avant, table_apres)

# 4. Lien entre I3 et I4
chisq.test(table(data.opref$I3_prob_sante, data.opref$I4_limitation_prob_sante))
assocstats(table(data.opref$I3_prob_sante, data.opref$I4_limitation_prob_sante))$cramer

chisq.test(table(opref.imp_df$I3_prob_sante, opref.imp_df$I4_limitation_prob_sante))
assocstats(table(opref.imp_df$I3_prob_sante, opref.imp_df$I4_limitation_prob_sante))$cramer

prop.table(table(data.opref$I3_prob_sante, data.opref$I4_limitation_prob_sante),
           margin=1)

prop.table(table(opref.imp_df$I3_prob_sante, opref.imp_df$I4_limitation_prob_sante),
           margin=1)

# 5. Représentation graphique
I4_df <- rbind(data.frame(imputation = "avant", I4 = data.I4$I4_limitation_prob_sante),
               data.frame(imputation = "après", I4 = data.imp.I4$I4_limitation_prob_sante))

# créer un diagramme en barres pour visualiser la distribution de la variable I1_etat_sante
ggplot(I4_df, aes(x = I4, fill = imputation)) + 
  geom_bar(position = "dodge") +
  labs(title = "Distribution de la variable I1_etat_sante",
       x = "I4_limitation_prob_sante",
       y = "Nombre d'individus") +
  theme_bw()