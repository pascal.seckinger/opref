# Validation croisée
library(StatMatch)
library(clue)

library(naniar)
library(caret)

set.seed(42)

load(file = "data/opref_nrp.rda")

###################################################################
# Validation de l'imputation des données du bloc I avec StatMatch #
###################################################################

# 1. Dataframe avec les données non imputées
data_df <- opref_df %>%
  select(-Date_inter, -Nom.image.unique, -Pays_Nai6, -ID_questionnaire,
         -MDP_questionnaire,
         -A0, -date_numerisation, -Type, -CODE_CENTRE, -vague,
         -Centre_Region, -Centre_Departement, -Centre_adresse, -Centre_network,
         -Centre_CP, -Centre_department_CP,-Numerisation_Annee,
         -Numerisation_Mois,-Naissance_Mois)

# 2. Création des données manquantes
# 2.1 Proportion de valeurs manquantes dans le bloc I
bloc_I <- names(data_df)[str_starts(names(data_df),"I")]
vis_miss(data_df[,bloc_I]) #14,7% de données manquantes
prop <- 0.147

# 2.2 dataframe sans valeurs manquantes dans le bloc I
dataI_df <- data_df %>% filter(complete.cases(select(., all_of(bloc_I))))
# vis_miss(dataI_df[,bloc_I])


# 2.3 Sélection aléatoire des indices de lignes
n_rows <- nrow(dataI_df)
selected_rows <- sample(1:n_rows, round(prop * n_rows))

# 2.4 Dataframe avec données manquantes
dataI.imp_df <- dataI_df
dataI.imp_df[selected_rows, bloc_I] <- NA

# 3. Imputation des valeurs manquantes
# 3.1 Dataframe des donneurs et des receveurs
receveurs <- which(rowSums(is.na(dataI.imp_df %>% select(all_of(bloc_I)))) > 0)
donneurs <- which(rowSums(is.na(dataI.imp_df %>% select(all_of(bloc_I)))) == 0)

data.rec <- dataI.imp_df[receveurs,]
data.don <- dataI.imp_df[donneurs,]

imp.NND <- NND.hotdeck(data.rec = data.rec, data.don = data.don,
                       match.vars = bloc_I, dist.fun="Gower", constrained = TRUE)

# 3.2 Imputation des valeurs manquantes
mts.ids_df <- as.data.frame(imp.NND$mtc.ids) %>%
  mutate(across(c(rec.id, don.id), as.numeric))

for (i in 1:nrow(mts.ids_df)) {
  rec <- mts.ids_df$rec.id[i]
  don <- mts.ids_df$don.id[i]
  var_imp <- bloc_I[which(is.na(dataI.imp_df[rec, bloc_I]))]
  dataI.imp_df[rec, var_imp] <- dataI.imp_df[don, var_imp]
}

# 4. Résultats
table(dataI_df[selected_rows, "I3_prob_sante"] == dataI.imp_df[selected_rows, "I3_prob_sante"])

cf <- caret::confusionMatrix(data=dataI.imp_df[selected_rows, "I3_prob_sante"],
                             reference=dataI_df[selected_rows, "I3_prob_sante"])
print(cf)
