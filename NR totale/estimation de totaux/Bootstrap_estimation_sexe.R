rm(list=ls())

library(tidyverse)
library(readxl)

## Chargement des fichiers du MC
load(file="data/bootstrap.rda")

## Chargement du fichier des individus répondants
load(file="data/opref_imp_sexe.rda")
data_cleaned <-opref.imp_df %>%
  select(CODE_CENTRE,Age,Sexe)
colnames(data_cleaned)[1]<-"Code_du_centre"


#############################################
#############################################
# On va estimer certains totaux à partir de notre échantillon
#
######################################################################
# Estimation du ratio hommes/femmes
######################################################################
#
# Les données groupées donnent une probabilité de réponse par individu
# valeurs réelles
ntot_hommes_reel <- sum(centres_df$Hommes)
ntot_femmes_reel <- sum(centres_df$Femmes)
ratio_hf_reel <- ntot_hommes_reel/ntot_femmes_reel

# Estimation du nombre total de BPI
# à calculer
ntot_hommes_estim<-matrix(nrow=3,ncol=ncol(retirage_bs_df)-1)
ntot_femmes_estim<-matrix(nrow=3,ncol=ncol(retirage_bs_df)-1)
ratio_hf_estim<-matrix(nrow=3,ncol=ncol(retirage_bs_df)-1)


for (i in 1:(ncol(retirage_bs_df)-1)){
  data2<-inner_join(data_cleaned,poids_centres_repondants_calage_bs_df[,c(1,i+1)],by="Code_du_centre")
  data2<-inner_join(data2,proba_individus_centre_bs_df[,c(1,i+1)],by="Code_du_centre")
  data2<-inner_join(data2,proba_individus_dept_bs_df[,c(1,i+1)],by="Code_du_centre")
  data2<-inner_join(data2,proba_individus_region_bs_df[,c(1,i+1)],by="Code_du_centre")
  
  data2[is.na(data2)]<-0
  colnames(data2)[4:7]<-c("poids_centre_calage","proba_indiv_centre",
                          "proba_indiv_dept","proba_indiv_region")
  for (k in 1:3){
    ntot_hommes_estim[k,i] <- sum((data2$Sexe == "Homme")*
                             data2$poids_centre_calage/data2[,4+k])
    ntot_femmes_estim[k,i] <- sum((data2$Sexe == "Femme")*
                              data2$poids_centre_calage/data2[,4+k])
    ratio_hf_estim[k,i]<-ntot_hommes_estim[k,i]/ntot_femmes_estim[k,i]
  }
}
result_centre_mc<-data.frame(ntot_hommes_estim[1,],ntot_femmes_estim[1,],
                             ratio_hf_estim[1,])
colnames(result_centre_mc)<-c("ntot_hommes_estim","ntot_femmes_estim",
                              "ratio_hf_estim")
result_dept_mc<-data.frame(ntot_hommes_estim[2,],ntot_femmes_estim[2,],
                           ratio_hf_estim[2,])
colnames(result_dept_mc)<-c("ntot_hommes_estim","ntot_femmes_estim",
                              "ratio_hf_estim")
result_region_mc<-data.frame(ntot_hommes_estim[3,],ntot_femmes_estim[3,],
                             ratio_hf_estim[3,])
colnames(result_region_mc)<-c("ntot_hommes_estim","ntot_femmes_estim",
                            "ratio_hf_estim")


ggplot(result_centre_mc)+
  theme_minimal()+
  geom_histogram(aes(x = ratio_hf_estim),binwidth=0.05,fill="blue")+
  xlab("Ratio h/f")+
  ylab("Nombre de simulations")+
  theme(plot.title = element_text(hjust = 0.5),
        plot.subtitle = element_text(hjust = 0.5))+
  ggtitle("Distribution des estimations du ratio h/f",
          subtitle="")

ratio_hf_reel
ratio_hf_estim_centre<-mean(result_centre_mc$ratio_hf_estim)
var_ratio_hf_estim_centre<-var(result_centre_mc$ratio_hf_estim)^0.5
ratio_hf_estim_dept<-mean(result_dept_mc$ratio_hf_estim)
var_ratio_hf_estim_dept<-var(result_dept_mc$ratio_hf_estim)^0.5
ratio_hf_estim_region<-mean(result_region_mc$ratio_hf_estim)
var_ratio_hf_estim_region<-var(result_region_mc$ratio_hf_estim)^0.5


######################################################################
# Estimation de la proportion de mineurs
######################################################################
#
# Les données groupées donnent une probabilité de réponse par individu
# valeurs réelles
ntot_mineurs_reel <- sum(centres_df$"0_17_ans")
ntot_bpi_reel <- sum(centres_df$Nombre_de_BPI)
ratio_mineurs_reel <- ntot_mineurs_reel/ntot_bpi_reel

# Estimation du nombre total de BPI
# à calculer
ntot_mineurs_estim<-matrix(nrow=3,ncol=ncol(retirage_bs_df)-1)
ntot_bpi_estim<-matrix(nrow=3,ncol=ncol(retirage_bs_df)-1)
ratio_mineurs_estim<-matrix(nrow=3,ncol=ncol(retirage_bs_df)-1)


for (i in 1:(ncol(retirage_bs_df)-1)){
  data2<-inner_join(data_cleaned,poids_centres_repondants_calage_bs_df[,c(1,i+1)],by="Code_du_centre")
  data2<-inner_join(data2,proba_individus_centre_bs_df[,c(1,i+1)],by="Code_du_centre")
  data2<-inner_join(data2,proba_individus_dept_bs_df[,c(1,i+1)],by="Code_du_centre")
  data2<-inner_join(data2,proba_individus_region_bs_df[,c(1,i+1)],by="Code_du_centre")
  
  data2[is.na(data2)]<-0
  colnames(data2)[4:7]<-c("poids_centre_calage","proba_indiv_centre",
                          "proba_indiv_dept","proba_indiv_region")
  for (k in 1:3){
    ntot_mineurs_estim[k,i] <- sum((data2$Age < 18)*
                                    data2$poids_centre_calage/data2[,4+k])
    ntot_bpi_estim[k,i] <- sum(data2$poids_centre_calage/data2[,4+k])
    ratio_mineurs_estim[k,i]<-ntot_mineurs_estim[k,i]/ntot_bpi_estim[k,i]
  }
}
result_centre_mc<-data.frame(ntot_mineurs_estim[1,],ntot_bpi_estim[1,],
                             ratio_mineurs_estim[1,])
colnames(result_centre_mc)<-c("ntot_mineurs_estim","ntot_bpi_estim",
                              "ratio_mineurs_estim")
result_dept_mc<-data.frame(ntot_mineurs_estim[2,],ntot_bpi_estim[2,],
                           ratio_mineurs_estim[2,])
colnames(result_dept_mc)<-c("ntot_mineurs_estim","ntot_bpi_estim",
                            "ratio_mineurs_estim")
result_region_mc<-data.frame(ntot_mineurs_estim[3,],ntot_bpi_estim[3,],
                             ratio_mineurs_estim[3,])
colnames(result_region_mc)<-c("ntot_mineurs_estim","ntot_bpi_estim",
                              "ratio_mineurs_estim")


ggplot(result_centre_mc)+
  theme_minimal()+
  geom_histogram(aes(x = ratio_mineurs_estim),binwidth=0.005,fill="blue")+
  xlab("proportion de mineurs")+
  ylab("Nombre de simulations")+
  theme(plot.title = element_text(hjust = 0.5),
        plot.subtitle = element_text(hjust = 0.5))+
  ggtitle("Distribution des estimations de la proportion de mineurs",
          subtitle="")

ratio_mineurs_reel
ratio_mineurs_estim_centre<-mean(result_centre_mc$ratio_mineurs_estim)
var_ratio_mineurs_estim_centre<-var(result_centre_mc$ratio_mineurs_estim)^0.5
ratio_mineurs_estim_dept<-mean(result_dept_mc$ratio_mineurs_estim)
var_ratio_mineurs_estim_dept<-var(result_dept_mc$ratio_mineurs_estim)^0.5
ratio_mineurs_estim_region<-mean(result_region_mc$ratio_mineurs_estim)
var_ratio_mineurs_estim_region<-var(result_region_mc$ratio_mineurs_estim)^0.5
