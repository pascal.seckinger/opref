# Projet statistique 2ème année ENSAI : Enquête Origine et Perspectives des Réfugiés en France (OPReF) – traitement de la non réponse

## 1. Présentation du sujet et des données et contexte
L’enquête Origine et Perspective des Réfugiés en France (OPReF) a été réalisée et administrée par l’Ecole d’Economie de Paris afin de mieux appréhender la population des bénéficiaires de la protection internationale dans le but notamment d’élaborer des politiques publiques plus efficaces destinées à leur intégration.
Cette enquête, financée par le Direction Générale des Etrangers en France et l’Institut Convergence Migration, a été administrée en 9 langues de novembre 2020 à Mai 2021 après une phase de conception et de pilote. L’année 2021/2022 a été consacrée à la numérisation et la rétro-traduction des questionnaires, ainsi que l’apurement des données. Le projet entre dans sa dernière phase en 2023, celle du redressement statistique qui doit conduire à son exploitation à compter de l’automne par les chercheurs et les institutions publiques.

## 2. Présentation du sujet et des données
L’objectif de ce projet est le traitement de la non réponse de l’enquête OPReF. La méthodologie choisie pour réaliser ce projet fait partie intégrante du sujet. L’ensemble des documents nécessaires à la création des poids sera mis à disposition du groupe. L’algorithme de tirage, la base de données initiale, les variables de stratification etc ...
En France, les demandeurs d’asile bénéficient dans certaines conditions du dispositif national d’accueil (DNA) qui leur offre l’accès à un hébergement administré par une association ou un organisme para-publique. Une fois que le statut de réfugié ou de protégé subsidiaire leur a été reconnu, ils bénéficient de la protection internationale de la France (BPI) et continuent d’être éligibles à un logement du DNA pendant 3 mois, renouvelable une fois.
L’enquête OPReF a été administrée aux BPI qui bénéficient d’un logement dans le DNA, en partie pour des raisons pratiques d’administration de l’enquête : leur adresse est connue et ils bénéficient généralement d’un accompagnement social. L’enquête à notamment pu s’appuyer sur le vaste réseau d’associations qui accompagnent les demandeurs d’asile et les BPI pour assurer un taux de réponse satisfaisant.
Le questionnaire de l’enquête OPReF comporte 12 blocs sur un recto-verso de page A4 pour environ 60 questions. Il a été traduit en 9 langues et auto-administré en version papier et en ligne. Envoyé à 10 000 BPI hébergés dans le DNA, 2632 réponses ont été collectées pour une population totale d’environ 18500 individus hébergé.
L’échantillonnage a été réalisé en collaboration avec le Service Statistique Ministériel (SSM) de la Direction Générale des Etrangers en France (DGEF) du Ministère de l’Intérieur, qui dispose de l’expertise métier pour réaliser ce type de tirage. Les données ont été traduites en français et apurées par l’équipe de l’Ecole d’Economie de Paris de septembre 2021 à septembre 2022.

## 3. Utilisation du code

### Appurement des données
1. Le script `apurement/nettoyage_data.R` nettoie les données et enregistre le résultat dans le fichier `data/donnees_apurees.Rdata`. Les données nettoyées sont stockées dans un dataframe nommé `opref_df`.
2. Le script `apurement/apurement_passe2.R` vérifie la cohérence des données et enregistre le résultat dans le fichier `data/donnees_opref.Rdata`. Les données vérifiées sont également stockées dans un dataframe nommé `opref_df`.
3. Le script `apurement_centres.R` renomme les variables de la base de sondage de façon cohérente et explicite et enregiste le résultat dans le fichier `data/centres.rda`. Les données nettoyées sont stockées dans un dataframe nommé `centres_df`.

### Prétraitement pour la non réponse partielle
Le script `NR partielle/prétraitement.R` réalise le prétraitement des données. Il calcule l'âge des individus à partir de leur date de naissance lorsqu'il est possible et que cela n'a pas déjà été effectué, et complète également la catégorie d'âge de ces individus. Il affecte également à chaque individu un nombre aléatoire compris entre 0 et 1 dans la variable `alea` qui sert à l'imputation aléatoire respectant la distribution. Finalement il retire les individus avec un taux de non réponse trop élevé, c'est à dire supérieur à 50%.

Les données prétraitées sont stockées dans le fichier `data/opref_imp.rda`. Ce fichier contient deux dataframes : `opref_df`, qui correspond aux données nettoyées et vérifiées, auxquelles nous avons ajouté l'âge des individus ; et `opref.imp_df`, qui correspond aux données ayant subi l'ensemble des opérations de prétraitement.

### Imputation
1. Les scripts utilisant l'imputation aléatoire suivant la distribution sont dans le dossier `NR partielle/imputation/aleatoire` et utilise les fonctions du script `NR partielle/fonctions/aleatoire/aleatoire.R`. Voici un exemple d'utilisation :
```
opref_df$variable_a_imputer <- aleatoireImp(opref_df, variable_a_imputer)
```
2. Les scripts utilisant la méthode hotdeck sont dans le dossier `NR partielle/imputation/hotdeck` et utilise les fonctions du script `NR partielle/fonctions/hotdeck/hotdeck.R`. Voici un exemple d'utilisation :
```
sim_mat <- hotdeckMat(opref_df, bloc_de_variables)
opref_df <- hotdeckImp(opref_df, bloc_de_variables, sim_mat)
```

### Prétraitement pour la non réponse totale
1. Le script `classes_age.R` calcule, pour chaque centre, la proportion de BPI dans chaque catégorie d'âge et enregistre le résultat dans le fichier `data/classes_age.rda`. Les données sont stockées dans un dataframe nommé `classes_age_df`.
2. Le script `langue_origine.R` calcule, pour chaque centre et chaque langue proposée dans les questionnaires, la proportion de BPI parlant potentiellement cette langue à partir des nationalités, ainsi que la proportion de BPI ne parlant aucune des langues proposées et enregistre le résultat dans le fichier `data/Non_representee.rda`. Les données sont stockées dans un dataframe nommé `Non_representee_df`.
3. Le script `langues_questionnaires.R` calcule, pour chaque centre, la proportion de questionnaires envoyés dans chaque langue lors de la première vague et enregistre le résultat dans le fichier `data/centres.langues.rda`. Les données sont stockées dans un dataframe nommé `centres.langues_df`.
4. Le script `langues_questionnaires_v2.R` calcule, pour chaque centre, la proportion de questionnaires envoyés dans chaque langue lors de la seconde vague et enregistre le résultat dans le fichier `data/centres.langues.v2.rda`. Les données sont stockées dans un dataframe nommé `centres.langues_v2_df`.
5. Le script `compare_langues.R` compare, pour chaque centre, la proportion de questionnaires envoyés dans cette langue lors de la première vague à la proportion de BPI pouvant potentiellement parler cette langue.
6. Le script `compare_vagues.R` compare, pour chaque centre, la proportion de questionnaires envoyés dans cette langue lors de la première vague à celle de la seconde vague pour estimer le turnover.
7. Le script `operateurs.R` calcule, pour chaque centre, la catégorie d'opérateur qui le gère. Les opérateurs sont catégorisés selon le nombre de centres gérés. Le script enregistre le résultat dans le fichier `data/operateurs.rda`. Les données sont stockées dans un dataframe nommé `operateurs_df`.
8. Le script `prop_femmes.R` calcule, pour chaque centre, la proportion de femmes et enregistre le résultat dans le fichier `data/prop_femmes.rda`. Les données sont stockées dans un dataframe nommé `prop_femmes_df`.
9. Le script `prop_refugies.R` calcule, pour chaque centre, la proportion de réfugiés et enregistre le résultat dans le fichier `data/prop_refugies.rda`. Les données sont stockées dans un dataframe nommé `prop_refugies_df`.
10. Le script `taux_de_reponse.R` calcule, pour chaque centre, département, région, type de centre et langue la proportion de répondants et enregistre le résultat dans le fichier `data/taux de réponse.rda`. Les données sont stockées dans des dataframe nommés `taux.de.reponse.par.centre_df`, `taux.de.reponse.par.departement_df`, `taux.de.reponse.par.type.centre_df`, `taux.de.reponse.par.region_df` et `taux.de.reponse.par.langue_df`.
11. Le script `taux_occupation.R` calcule, pour chaque centre, son taux d'occupation et enregistre le résultat dans le fichier `data/Taux_occupation.rda`. Les données sont stockées dans un dataframe nommé `Taux_occupation_df`.
12. Le script `zones_geographiques_origine.R` calcule, pour chaque centre, la proportion de BPI venant d'une zone géographique à partir des nationalités et enregistre le résultat dans le fichier `data/zones géographiques.rda`. Les données sont stockées dans un dataframe nommé `zones.geo_df`.
13. Le script `stat_desc_NR_totale.R` réalise des graphiques pour la phase d'exploration des données.

### Modélisation
1. Le script `NR_phase1_centres.R` effectue une régression logistique au niveau des centres répondants/non-répondants. Si le résultat avait été concluant, il aurait été enregistré dans le fichier `data/proba_centres_vague1_repondants.rda`. 
2. Le script `NR_phase1_centres_arbre.R` utilise un arbre de classification au niveau des centres répondants/non-répondants. Si le résultat avait été concluant, il aurait été enregistré dans le fichier `data/proba_centres_vague1_repondants_arbre.rda`. 
3. Le script `NR_phase2_individus.R` effectue une régression logistique à données groupées au niveau des centres, département et régions et détermine des groupes homogènes de réponse pour chaque niveau. Le résultat est enregistré dans le fichier `data/proba_individus.rda` dans le dataframe `proba_individus_df`. 
4. Le script `NR_phase2_individus_arbre.R` utilise un arbre de régression au niveau des centres et département et détermine des groupes homogènes de réponse pour chaque niveau. Le résultat est enregistré dans le fichier `data/proba_individus_arbre.rda` dans le dataframe `proba_individus_arbre_df`. 

Pour lancer une modélisation, il faut au préalable exécuter les scripts `apurement_centres.R`, `nettoyage_data.R` et `apurement_passe2.R` du dossier `apurement` et `prétraitement.R` du dossier `NR partielle` afin de mettre en forme les données et classer les individus peu répondants en non-répondants.

### Calage
1. Le script `calage_centres.R` effectue un calage sur marges au niveau des centres répondants en utilisant les 33 variables d'équilibrage du plan de sondage. Le résultat est enregistré dans le fichier `data/poids_centres_repondants_calage.rda` dans le dataframe `poids_centres_repondants_calage_df`. 

### Estimation de totaux
1. Le script `estimation_post_calage_centres_repondants.R` effectue une estimation de quelques totaux à l'issue du calage au niveau des centres répondants.
2. Le script `estimation_post_phase2.R` effectue une estimation de quelques totaux à l'issue de la phase "individus" (régression logistique ou arbre de régression).
3. Le script `bootstrap_calage.R` tire des rééchantillons et procède à la phase de calage sur marges sur ces rééchantillons. Les rééchantillons et les poids résultants sont enregistrés dans le fichier `data/bootstrap.rda` dans les dataframes `retirage_bs_df` et `poids_centres_repondants_calage_bs_df`.
4. Le script `bootstrap_grh.R` recalcule le taux de réponse dans chaque groupe homogène de réponse pour les rééchantillons. Le calcul est fait au niveau des centres, départements et régions. Les résultats sont enregistrés dans le fichier `data/bootstrap.rda` dans les dataframes `proba_individus_centre_bs_df`, `proba_individus_dept_bs_df` et `proba_individus_region_bs_df`.
5. Le script `bootstrap_estimation.R` utilise, pour chaque rééchantillon, les poids issus du calage et des groupes homogènes de réponse pour effectuer une estimation de totaux de variables liées au centre (i.e. ne dépendant pas de l'imputation de la non-réponse partielle)
6. Le script `bootstrap_estimation_sexe.R` utilise, pour chaque rééchantillon, les poids issus du calage et des groupes homogènes de réponse pour effectuer une estimation de totaux de variables individuelles (i.e. dépendant de l'imputation de la non-réponse partielle)